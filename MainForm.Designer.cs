﻿using WebCamClient.Properties;

namespace WebCamClient
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.image = new System.Windows.Forms.PictureBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.numTilt = new DevExpress.XtraEditors.SpinEdit();
			this.numPan = new DevExpress.XtraEditors.SpinEdit();
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.button13 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.button21 = new System.Windows.Forms.Button();
			this.button22 = new System.Windows.Forms.Button();
			this.button23 = new System.Windows.Forms.Button();
			this.button24 = new System.Windows.Forms.Button();
			this.button25 = new System.Windows.Forms.Button();
			this.button26 = new System.Windows.Forms.Button();
			this.button27 = new System.Windows.Forms.Button();
			this.button29 = new System.Windows.Forms.Button();
			this.button30 = new System.Windows.Forms.Button();
			this.button31 = new System.Windows.Forms.Button();
			this.button32 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button5 = new System.Windows.Forms.Button();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.image)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numTilt.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPan.Properties)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// image
			// 
			this.image.Location = new System.Drawing.Point(13, 25);
			this.image.Name = "image";
			this.image.Size = new System.Drawing.Size(640, 480);
			this.image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.image.TabIndex = 0;
			this.image.TabStop = false;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(325, 536);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(20, 20);
			this.button1.TabIndex = 1;
			this.button1.Text = "^";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(354, 561);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(20, 20);
			this.button2.TabIndex = 2;
			this.button2.Text = ">";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(297, 561);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(20, 20);
			this.button3.TabIndex = 3;
			this.button3.Text = "<";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(325, 588);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(20, 20);
			this.button4.TabIndex = 4;
			this.button4.Text = "v";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// numTilt
			// 
			this.numTilt.EditValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.numTilt.Location = new System.Drawing.Point(86, 559);
			this.numTilt.Name = "numTilt";
			this.numTilt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.numTilt.Size = new System.Drawing.Size(45, 20);
			this.numTilt.TabIndex = 5;
			// 
			// numPan
			// 
			this.numPan.EditValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.numPan.Location = new System.Drawing.Point(86, 533);
			this.numPan.Name = "numPan";
			this.numPan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.numPan.Size = new System.Drawing.Size(45, 20);
			this.numPan.TabIndex = 6;
			// 
			// labelControl1
			// 
			this.labelControl1.Location = new System.Drawing.Point(10, 536);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(66, 13);
			this.labelControl1.TabIndex = 7;
			this.labelControl1.Text = Resources.LabelHorizontalDegree_Text_en;
			// 
			// labelControl2
			// 
			this.labelControl2.Location = new System.Drawing.Point(14, 562);
			this.labelControl2.Name = "labelControl2";
			this.labelControl2.Size = new System.Drawing.Size(62, 13);
			this.labelControl2.TabIndex = 8;
			this.labelControl2.Text = Resources.LabelVerticalDegree_Text_en;
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(398, 578);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(100, 28);
			this.button6.TabIndex = 11;
			this.button6.Text = Resources.EnableIR_ButtonTxt_en;
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(398, 536);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(100, 28);
			this.button7.TabIndex = 12;
			this.button7.Text = Resources.ButtonSwing_Text_en;
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(6, 19);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(27, 27);
			this.button8.TabIndex = 13;
			this.button8.Text = "1";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(35, 19);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(27, 27);
			this.button9.TabIndex = 14;
			this.button9.Text = "2";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(64, 19);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(27, 27);
			this.button10.TabIndex = 15;
			this.button10.Text = "3";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(93, 19);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(27, 27);
			this.button11.TabIndex = 16;
			this.button11.Text = "4";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button12
			// 
			this.button12.Location = new System.Drawing.Point(122, 19);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(27, 27);
			this.button12.TabIndex = 17;
			this.button12.Text = "5";
			this.button12.UseVisualStyleBackColor = true;
			this.button12.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(670, 24);
			this.menuStrip1.TabIndex = 18;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = Resources.MenuFileItem_Text_en;
			// 
			// settingsToolStripMenuItem
			// 
			this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
			this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.settingsToolStripMenuItem.Text = Resources.MenuSettingsItem_Text_en;
			this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.exitToolStripMenuItem.Text = Resources.MenuExitItem_Text_en;
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// button13
			// 
			this.button13.Location = new System.Drawing.Point(122, 48);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(27, 27);
			this.button13.TabIndex = 23;
			this.button13.Text = "10";
			this.button13.UseVisualStyleBackColor = true;
			this.button13.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button14
			// 
			this.button14.Location = new System.Drawing.Point(93, 48);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(27, 27);
			this.button14.TabIndex = 22;
			this.button14.Text = "9";
			this.button14.UseVisualStyleBackColor = true;
			this.button14.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button15
			// 
			this.button15.Location = new System.Drawing.Point(64, 48);
			this.button15.Name = "button15";
			this.button15.Size = new System.Drawing.Size(27, 27);
			this.button15.TabIndex = 21;
			this.button15.Text = "8";
			this.button15.UseVisualStyleBackColor = true;
			this.button15.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button16
			// 
			this.button16.Location = new System.Drawing.Point(35, 48);
			this.button16.Name = "button16";
			this.button16.Size = new System.Drawing.Size(27, 27);
			this.button16.TabIndex = 20;
			this.button16.Text = "7";
			this.button16.UseVisualStyleBackColor = true;
			this.button16.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button17
			// 
			this.button17.Location = new System.Drawing.Point(6, 48);
			this.button17.Name = "button17";
			this.button17.Size = new System.Drawing.Size(27, 27);
			this.button17.TabIndex = 19;
			this.button17.Text = "6";
			this.button17.UseVisualStyleBackColor = true;
			this.button17.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button18
			// 
			this.button18.Location = new System.Drawing.Point(122, 77);
			this.button18.Name = "button18";
			this.button18.Size = new System.Drawing.Size(27, 27);
			this.button18.TabIndex = 28;
			this.button18.Text = "15";
			this.button18.UseVisualStyleBackColor = true;
			this.button18.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button19
			// 
			this.button19.Location = new System.Drawing.Point(93, 77);
			this.button19.Name = "button19";
			this.button19.Size = new System.Drawing.Size(27, 27);
			this.button19.TabIndex = 27;
			this.button19.Text = "14";
			this.button19.UseVisualStyleBackColor = true;
			this.button19.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button20
			// 
			this.button20.Location = new System.Drawing.Point(64, 77);
			this.button20.Name = "button20";
			this.button20.Size = new System.Drawing.Size(27, 27);
			this.button20.TabIndex = 26;
			this.button20.Text = "13";
			this.button20.UseVisualStyleBackColor = true;
			this.button20.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button21
			// 
			this.button21.Location = new System.Drawing.Point(35, 77);
			this.button21.Name = "button21";
			this.button21.Size = new System.Drawing.Size(27, 27);
			this.button21.TabIndex = 25;
			this.button21.Text = "12";
			this.button21.UseVisualStyleBackColor = true;
			this.button21.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button22
			// 
			this.button22.Location = new System.Drawing.Point(6, 77);
			this.button22.Name = "button22";
			this.button22.Size = new System.Drawing.Size(27, 27);
			this.button22.TabIndex = 24;
			this.button22.Text = "11";
			this.button22.UseVisualStyleBackColor = true;
			this.button22.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button23
			// 
			this.button23.Location = new System.Drawing.Point(122, 106);
			this.button23.Name = "button23";
			this.button23.Size = new System.Drawing.Size(27, 27);
			this.button23.TabIndex = 33;
			this.button23.Text = "20";
			this.button23.UseVisualStyleBackColor = true;
			this.button23.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button24
			// 
			this.button24.Location = new System.Drawing.Point(93, 106);
			this.button24.Name = "button24";
			this.button24.Size = new System.Drawing.Size(27, 27);
			this.button24.TabIndex = 32;
			this.button24.Text = "19";
			this.button24.UseVisualStyleBackColor = true;
			this.button24.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button25
			// 
			this.button25.Location = new System.Drawing.Point(64, 106);
			this.button25.Name = "button25";
			this.button25.Size = new System.Drawing.Size(27, 27);
			this.button25.TabIndex = 31;
			this.button25.Text = "18";
			this.button25.UseVisualStyleBackColor = true;
			this.button25.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button26
			// 
			this.button26.Location = new System.Drawing.Point(35, 106);
			this.button26.Name = "button26";
			this.button26.Size = new System.Drawing.Size(27, 27);
			this.button26.TabIndex = 30;
			this.button26.Text = "17";
			this.button26.UseVisualStyleBackColor = true;
			this.button26.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button27
			// 
			this.button27.Location = new System.Drawing.Point(6, 106);
			this.button27.Name = "button27";
			this.button27.Size = new System.Drawing.Size(27, 27);
			this.button27.TabIndex = 29;
			this.button27.Text = "16";
			this.button27.UseVisualStyleBackColor = true;
			this.button27.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button29
			// 
			this.button29.Location = new System.Drawing.Point(93, 135);
			this.button29.Name = "button29";
			this.button29.Size = new System.Drawing.Size(27, 27);
			this.button29.TabIndex = 37;
			this.button29.Text = "24";
			this.button29.UseVisualStyleBackColor = true;
			this.button29.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button30
			// 
			this.button30.Location = new System.Drawing.Point(64, 135);
			this.button30.Name = "button30";
			this.button30.Size = new System.Drawing.Size(27, 27);
			this.button30.TabIndex = 36;
			this.button30.Text = "23";
			this.button30.UseVisualStyleBackColor = true;
			this.button30.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button31
			// 
			this.button31.Location = new System.Drawing.Point(35, 135);
			this.button31.Name = "button31";
			this.button31.Size = new System.Drawing.Size(27, 27);
			this.button31.TabIndex = 35;
			this.button31.Text = "22";
			this.button31.UseVisualStyleBackColor = true;
			this.button31.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// button32
			// 
			this.button32.Location = new System.Drawing.Point(6, 135);
			this.button32.Name = "button32";
			this.button32.Size = new System.Drawing.Size(27, 27);
			this.button32.TabIndex = 34;
			this.button32.Text = "21";
			this.button32.UseVisualStyleBackColor = true;
			this.button32.Click += new System.EventHandler(this.PresetButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button8);
			this.groupBox1.Controls.Add(this.button9);
			this.groupBox1.Controls.Add(this.button29);
			this.groupBox1.Controls.Add(this.button10);
			this.groupBox1.Controls.Add(this.button30);
			this.groupBox1.Controls.Add(this.button11);
			this.groupBox1.Controls.Add(this.button31);
			this.groupBox1.Controls.Add(this.button12);
			this.groupBox1.Controls.Add(this.button32);
			this.groupBox1.Controls.Add(this.button17);
			this.groupBox1.Controls.Add(this.button23);
			this.groupBox1.Controls.Add(this.button16);
			this.groupBox1.Controls.Add(this.button24);
			this.groupBox1.Controls.Add(this.button15);
			this.groupBox1.Controls.Add(this.button25);
			this.groupBox1.Controls.Add(this.button14);
			this.groupBox1.Controls.Add(this.button26);
			this.groupBox1.Controls.Add(this.button13);
			this.groupBox1.Controls.Add(this.button27);
			this.groupBox1.Controls.Add(this.button22);
			this.groupBox1.Controls.Add(this.button18);
			this.groupBox1.Controls.Add(this.button21);
			this.groupBox1.Controls.Add(this.button19);
			this.groupBox1.Controls.Add(this.button20);
			this.groupBox1.Location = new System.Drawing.Point(504, 515);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(157, 173);
			this.groupBox1.TabIndex = 39;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = Resources.GroupPresets_Title_en;
			// 
			// button5
			// 
			this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
			this.button5.Location = new System.Drawing.Point(323, 559);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(25, 25);
			this.button5.TabIndex = 40;
			this.button5.Text = "";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click_1);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.aboutToolStripMenuItem.Text = Resources.MenuAboutItem_Text_en;
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(670, 698);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.labelControl2);
			this.Controls.Add(this.labelControl1);
			this.Controls.Add(this.numPan);
			this.Controls.Add(this.numTilt);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.image);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MainMenuStrip = this.menuStrip1;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.ShowIcon = false;
			this.Text = Resources.MainForm_Title_en;
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.image)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numTilt.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPan.Properties)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox image;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private DevExpress.XtraEditors.SpinEdit numTilt;
		private DevExpress.XtraEditors.SpinEdit numPan;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button button23;
		private System.Windows.Forms.Button button24;
		private System.Windows.Forms.Button button25;
		private System.Windows.Forms.Button button26;
		private System.Windows.Forms.Button button27;
		private System.Windows.Forms.Button button29;
		private System.Windows.Forms.Button button30;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.Button button32;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
	}
}


﻿namespace WebCamClient
{
	public enum PanTiltSingleMoveParameters
	{
		UpLeft=0,
		Up=1,
		UpRight=2,
		Left=3,
		Home=4,
		Right=5,
		DownLeft=6,
		Down=7,
		DownRight=8
	}

	public enum PanTileSwingModeParameters
	{
		Stop=0,
		HorizontalScan=1,
		PresetScan=2
	}
}

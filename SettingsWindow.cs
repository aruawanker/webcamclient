﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebCamClient.Properties;

namespace WebCamClient
{
	public partial class SettingsWindow : Form
	{
		public SettingsWindow()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				Settings.Default.password = txtPassword.Text;
				Settings.Default.url = txtUrl.Text;
				Settings.Default.user = txtUser.Text;
				Settings.Default.Save();
				MessageBox.Show(Resources.SettingsSaved_MsgBoxTxt_en, Resources.SettingsSaved_MsgBoxTitle_en, MessageBoxButtons.OK);
				Close();
			}
			catch
			{
				MessageBox.Show(Resources.SettingsNotSaved_MsgBoxTxt_en, Resources.SettingsNotSaved_MsgBoxTitle_en, MessageBoxButtons.OK);
			}
			
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Close();
		}

		public string Url
		{
			get { return txtUrl.Text; }
		}


		public string Password
		{
			get { return txtPassword.Text; }
		}

		public string User
		{
			get { return txtUser.Text; }
		}

		private void SettingsWindow_Load(object sender, EventArgs e)
		{
			txtPassword.Text = Settings.Default.password;
			txtUrl.Text = Settings.Default.url;
			txtUser.Text = Settings.Default.user;
		}
	}
}
